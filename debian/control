Source: minetest
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Martin Quinson <mquinson@debian.org>,
 Markus Koschany <apo@debian.org>,
 Dominik George <natureshadow@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 gettext,
 libbz2-dev,
 libcurl4-gnutls-dev,
 libfreetype-dev,
 libglu1-mesa-dev,
 libgmp-dev,
 libjpeg-dev,
 libjsoncpp-dev,
 libleveldb-dev,
 libluajit-5.1-dev,
 libogg-dev,
 libopenal-dev,
 libpng-dev,
 libpq-dev,
 libspatialindex-dev,
 libsqlite3-dev,
 libvorbis-dev,
 libx11-dev,
 libxxf86vm-dev,
 pkgconf,
 postgresql-server-dev-all,
 zlib1g-dev,
 libsdl2-dev,
 doxygen,
 libxrandr-dev,
 mesa-common-dev,
 x11proto-xf86vidmode-dev,
 libzstd-dev
Standards-Version: 4.6.2
Homepage: https://www.minetest.net/
Vcs-Git: https://salsa.debian.org/games-team/minetest.git
Vcs-Browser: https://salsa.debian.org/games-team/minetest
Rules-Requires-Root: no

Package: minetest
Architecture: any
Depends:
 minetest-data (= ${source:Version}),
 sse2-support [i386],
 ${misc:Depends},
 ${shlibs:Depends}
Conflicts:
 minetestc55
Suggests:
 minetest-mod-moreblocks,
 minetest-mod-moreores,
 minetest-mod-pipeworks,
 minetest-server,
 minetestmapper
Description: Multiplayer infinite-world block sandbox
 Minetest is a minecraft-inspired game written from scratch and licensed
 under the LGPL (version 2.1 or later). It supports both survival and creative
 modes along with multiplayer support, dynamic lighting, and an "infinite" map
 generator.

Package: minetest-server
Architecture: any
Depends:
 adduser,
 minetest-data (= ${source:Version}),
 sse2-support [i386],
 ${misc:Depends},
 ${shlibs:Depends}
Pre-Depends:
 ${misc:Pre-Depends}
Description: Multiplayer infinite-world block sandbox (server)
 Minetest is a minecraft-inspired game written from scratch and licensed
 under the LGPL (version 2.1 or later). It supports both survival and creative
 modes along with multiplayer support, dynamic lighting, and an "infinite" map
 generator.
 .
 This is the minetest server package.

Package: minetest-data
Architecture: all
Multi-Arch: foreign
Depends:
 fonts-croscore,
 fonts-droid-fallback,
 ${misc:Depends}
Description: Multiplayer infinite-world block sandbox (data files)
 Minetest is a minecraft-inspired game written from scratch and licensed
 under the LGPL (version 2.1 or later). It supports both survival and creative
 modes along with multiplayer support, dynamic lighting, and an "infinite" map
 generator.
 .
 These are the files used by both client and server packages, such as
 the textures and other graphical files and the standard mods.
